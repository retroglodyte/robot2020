int stepPin = 5;

//left
int leftDirPin = 9;
int leftSleepPin = 7;
int leftEnablePin = 8;


//right
int rightDirPin = 12;
int rightSleepPin = 10;
int rightEnablePin = 11;



void setup() {
  pinMode(stepPin, OUTPUT);
  pinMode(leftDirPin, OUTPUT);
  pinMode(leftSleepPin, OUTPUT);
  pinMode(leftEnablePin, OUTPUT);
  pinMode(rightDirPin, OUTPUT);
  pinMode(rightSleepPin, OUTPUT);
  pinMode(rightEnablePin, OUTPUT);
  


  digitalWrite(stepPin, LOW);
  digitalWrite(leftDirPin, LOW);
  digitalWrite(leftSleepPin, HIGH);
  digitalWrite(leftEnablePin, LOW);
  digitalWrite(rightDirPin, HIGH);
  digitalWrite(rightSleepPin, HIGH);
  digitalWrite(rightEnablePin, LOW);


  delay(1000);
}

void loop() {

  //both
  digitalWrite(rightEnablePin, LOW);
  digitalWrite(leftEnablePin, LOW);
  for(int i = 0; i < 200; i++)
  {
    delay(5);
    digitalWrite(stepPin, HIGH);
    delay(5);
    digitalWrite(stepPin, LOW);
    
    delay(10);
  }


  //left
  digitalWrite(rightEnablePin, HIGH);
  digitalWrite(leftEnablePin, LOW);
  for(int i = 0; i < 200; i++)
  {
    delay(5);
    digitalWrite(stepPin, HIGH);
    delay(5);
    digitalWrite(stepPin, LOW);
    
    delay(10);
  }


  //right
  digitalWrite(rightEnablePin, LOW);
  digitalWrite(leftEnablePin, HIGH);
  for(int i = 0; i < 200; i++)
  {
    delay(5);
    digitalWrite(stepPin, HIGH);
    delay(5);
    digitalWrite(stepPin, LOW);
    
    delay(10);
  }
}
