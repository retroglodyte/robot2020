//There is a problem with the headlight. Probably with the cricuit.

//i2c sda on pin A4
//i2c scl on pin A5

int headlightPin = A3;

int stepPin = 5;

//left
int leftDirPin = 12;
int leftSleepPin = 10;
int leftEnablePin = 11;


//right
int rightDirPin = A2;
int rightSleepPin = A0;
int rightEnablePin = A1;



void setup() {
  pinMode(headlightPin, OUTPUT);
  digitalWrite(headlightPin, LOW);
  
  pinMode(stepPin, OUTPUT);
  pinMode(leftDirPin, OUTPUT);
  pinMode(leftSleepPin, OUTPUT);
  pinMode(leftEnablePin, OUTPUT);
  pinMode(rightDirPin, OUTPUT);
  pinMode(rightSleepPin, OUTPUT);
  pinMode(rightEnablePin, OUTPUT);
  


  digitalWrite(stepPin, LOW);
  digitalWrite(leftDirPin, LOW);
  digitalWrite(leftSleepPin, HIGH);
  digitalWrite(leftEnablePin, HIGH);
  digitalWrite(rightDirPin, LOW);
  digitalWrite(rightSleepPin, HIGH);
  digitalWrite(rightEnablePin, HIGH);


  delay(10000);

  Serial.begin(115200);
}

void loop() {

  char in = 'z';

  if(Serial.available())
    in = Serial.read();

  switch(in)
  {
    case 'w':
      forward();
      break;
    case 'a':
      left();
      break;
    case 'd':
      right();
      break;
    case 's':
      reverse();
      break;
    case 'l':
      flashLights();
      break;
    default:
      break;;
  }
  
  
}






void right()
{
  digitalWrite(rightEnablePin, HIGH);
  digitalWrite(leftEnablePin, LOW);
  for(int i = 0; i < 50; i++)
  {
    delay(3);
    digitalWrite(stepPin, HIGH);
    delay(3);
    digitalWrite(stepPin, LOW);
    
    delay(6);
  }  
  //idle both wheels
  digitalWrite(rightEnablePin, HIGH);
  digitalWrite(leftEnablePin, HIGH);
}

void left()
{
  digitalWrite(rightEnablePin, LOW);
  digitalWrite(leftEnablePin, HIGH);
  for(int i = 0; i < 50; i++)
  {
    delay(3);
    digitalWrite(stepPin, HIGH);
    delay(3);
    digitalWrite(stepPin, LOW);
    
    delay(6);
  }
  //idle both wheels
  digitalWrite(rightEnablePin, HIGH);
  digitalWrite(leftEnablePin, HIGH);
}

void forward()
{
  digitalWrite(rightEnablePin, LOW);
  digitalWrite(leftEnablePin, LOW);
  for(int i = 0; i < 50; i++)
  {
    delay(3);
    digitalWrite(stepPin, HIGH);
    delay(3);
    digitalWrite(stepPin, LOW);
    
    delay(6);
  }
  //idle both wheels
  digitalWrite(rightEnablePin, HIGH);
  digitalWrite(leftEnablePin, HIGH);
}

void flashLights()
{
  digitalWrite(headlightPin, HIGH);
  delay(2000);
  digitalWrite(headlightPin, LOW);
}

void reverse()
{
  digitalWrite(rightDirPin, HIGH);
  digitalWrite(leftDirPin, HIGH);


  digitalWrite(rightEnablePin, LOW);
  digitalWrite(leftEnablePin, LOW);
  for(int i = 0; i < 50; i++)
  {
    delay(3);
    digitalWrite(stepPin, HIGH);
    delay(3);
    digitalWrite(stepPin, LOW);
    
    delay(6);
  }
  //idle both wheels
  digitalWrite(rightEnablePin, HIGH);
  digitalWrite(leftEnablePin, HIGH);


  digitalWrite(rightDirPin, LOW);
  digitalWrite(leftDirPin, LOW);
  
}

