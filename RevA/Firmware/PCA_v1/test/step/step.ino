//There is a problem with the headlight. Probably with the cricuit.

//i2c sda on pin A4
//i2c scl on pin A5

int headlightPin = A3;

int stepPin = 5;

//left
int leftDirPin = 12;
int leftSleepPin = 10;
int leftEnablePin = 11;


//right
int rightDirPin = A2;
int rightSleepPin = A0;
int rightEnablePin = A1;



void setup() {
  pinMode(headlightPin, OUTPUT);
  digitalWrite(headlightPin, LOW);
  
  pinMode(stepPin, OUTPUT);
  pinMode(leftDirPin, OUTPUT);
  pinMode(leftSleepPin, OUTPUT);
  pinMode(leftEnablePin, OUTPUT);
  pinMode(rightDirPin, OUTPUT);
  pinMode(rightSleepPin, OUTPUT);
  pinMode(rightEnablePin, OUTPUT);
  


  digitalWrite(stepPin, LOW);
  digitalWrite(leftDirPin, LOW);
  digitalWrite(leftSleepPin, HIGH);
  digitalWrite(leftEnablePin, LOW);
  digitalWrite(rightDirPin, HIGH);
  digitalWrite(rightSleepPin, HIGH);
  digitalWrite(rightEnablePin, LOW);


  delay(1000);
}

void loop() {

  //both
  digitalWrite(rightEnablePin, LOW);
  digitalWrite(leftEnablePin, LOW);
  for(int i = 0; i < 200; i++)
  {
    delay(5);
    digitalWrite(stepPin, HIGH);
    delay(5);
    digitalWrite(stepPin, LOW);
    
    delay(10);
  }


  //left
  digitalWrite(rightEnablePin, HIGH);
  digitalWrite(leftEnablePin, LOW);
  for(int i = 0; i < 200; i++)
  {
    delay(5);
    digitalWrite(stepPin, HIGH);
    delay(5);
    digitalWrite(stepPin, LOW);
    
    delay(10);
  }


  //right
  digitalWrite(rightEnablePin, LOW);
  digitalWrite(leftEnablePin, HIGH);
  for(int i = 0; i < 200; i++)
  {
    delay(5);
    digitalWrite(stepPin, HIGH);
    delay(5);
    digitalWrite(stepPin, LOW);
    
    delay(10);
  }

  //idle both wheels
  digitalWrite(rightEnablePin, HIGH);
  digitalWrite(leftEnablePin, HIGH);

  //lights on for 5 seconds
  digitalWrite(headlightPin, HIGH);
  delay(2000);
  digitalWrite(headlightPin, LOW);
}
