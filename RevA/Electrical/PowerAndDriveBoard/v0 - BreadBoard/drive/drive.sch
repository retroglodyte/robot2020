EESchema Schematic File Version 4
LIBS:drive-cache
EELAYER 30 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 1 1
Title "Power Supply and Motor Control Board"
Date "2019-12-30"
Rev "000"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:Arduino_Nano_v3.x U2
U 1 1 5E07D7F5
P 2200 4600
F 0 "U2" H 2550 5700 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" H 2850 5600 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 2350 3650 50  0001 L CNN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 2200 3600 50  0001 C CNN
	1    2200 4600
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_A4988 U4
U 1 1 5E080446
P 8700 3500
F 0 "U4" H 9250 4050 50  0000 C CNN
F 1 "Pololu_Breakout_A4988" V 8800 3450 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 8975 2750 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 8800 3200 50  0001 C CNN
	1    8700 3500
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_A4988 U3
U 1 1 5E081523
P 6150 3500
F 0 "U3" H 6650 4050 50  0000 C CNN
F 1 "Pololu_Breakout_A4988" V 6250 3450 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 6425 2750 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 6250 3200 50  0001 C CNN
	1    6150 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J1
U 1 1 5E088206
P 2500 1700
F 0 "J1" H 2392 1793 50  0000 C CNN
F 1 "Conn_01x02_Female" H 2392 1794 50  0001 C CNN
F 2 "" H 2500 1700 50  0001 C CNN
F 3 "~" H 2500 1700 50  0001 C CNN
	1    2500 1700
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E09C878
P 2800 1900
F 0 "#PWR?" H 2800 1650 50  0001 C CNN
F 1 "GND" H 2805 1727 50  0000 C CNN
F 2 "" H 2800 1900 50  0001 C CNN
F 3 "" H 2800 1900 50  0001 C CNN
	1    2800 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 1800 2800 1800
$Comp
L power:+12V #PWR?
U 1 1 5E08B202
P 2800 1500
F 0 "#PWR?" H 2800 1350 50  0001 C CNN
F 1 "+12V" H 2815 1673 50  0000 C CNN
F 2 "" H 2800 1500 50  0001 C CNN
F 3 "" H 2800 1500 50  0001 C CNN
	1    2800 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 1800 2800 1900
$Comp
L power:GND #PWR?
U 1 1 5E0C047A
P 8800 4450
F 0 "#PWR?" H 8800 4200 50  0001 C CNN
F 1 "GND" H 8805 4277 50  0000 C CNN
F 2 "" H 8800 4450 50  0001 C CNN
F 3 "" H 8800 4450 50  0001 C CNN
	1    8800 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 4300 8700 4400
Wire Wire Line
	8700 4400 8800 4400
Wire Wire Line
	8800 4400 8800 4450
Wire Wire Line
	8900 4300 8900 4400
Wire Wire Line
	8900 4400 8800 4400
Connection ~ 8800 4400
$Comp
L power:GND #PWR?
U 1 1 5E0BFDE8
P 6250 4500
F 0 "#PWR?" H 6250 4250 50  0001 C CNN
F 1 "GND" H 6255 4327 50  0000 C CNN
F 2 "" H 6250 4500 50  0001 C CNN
F 3 "" H 6250 4500 50  0001 C CNN
	1    6250 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 4300 6150 4450
Wire Wire Line
	6350 4300 6350 4450
Wire Wire Line
	6150 4450 6250 4450
Wire Wire Line
	6250 4500 6250 4450
Connection ~ 6250 4450
Wire Wire Line
	6250 4450 6350 4450
NoConn ~ 5750 3800
NoConn ~ 5750 3900
NoConn ~ 5750 4000
NoConn ~ 8300 3800
NoConn ~ 8300 3900
NoConn ~ 8300 4000
$Comp
L power:+5V #PWR?
U 1 1 5E0CF6BB
P 8400 2650
F 0 "#PWR?" H 8400 2500 50  0001 C CNN
F 1 "+5V" H 8415 2823 50  0000 C CNN
F 2 "" H 8400 2650 50  0001 C CNN
F 3 "" H 8400 2650 50  0001 C CNN
	1    8400 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 3100 8150 3100
Wire Wire Line
	8150 3100 8150 2750
Wire Wire Line
	8150 2750 8400 2750
Wire Wire Line
	8400 2750 8400 2650
Wire Wire Line
	8700 2800 8700 2750
Wire Wire Line
	8700 2750 8400 2750
Connection ~ 8400 2750
$Comp
L power:+5V #PWR?
U 1 1 5E0D1EC1
P 5850 2550
F 0 "#PWR?" H 5850 2400 50  0001 C CNN
F 1 "+5V" H 5865 2723 50  0000 C CNN
F 2 "" H 5850 2550 50  0001 C CNN
F 3 "" H 5850 2550 50  0001 C CNN
	1    5850 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3100 5550 3100
Wire Wire Line
	5550 3100 5550 2700
Wire Wire Line
	5550 2700 5850 2700
Wire Wire Line
	6150 2700 6150 2800
Wire Wire Line
	5850 2550 5850 2700
Connection ~ 5850 2700
Wire Wire Line
	5850 2700 6150 2700
$Comp
L power:+12V #PWR?
U 1 1 5E0D5184
P 6350 2600
F 0 "#PWR?" H 6350 2450 50  0001 C CNN
F 1 "+12V" H 6365 2773 50  0000 C CNN
F 2 "" H 6350 2600 50  0001 C CNN
F 3 "" H 6350 2600 50  0001 C CNN
	1    6350 2600
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5E0D60B0
P 8900 2650
F 0 "#PWR?" H 8900 2500 50  0001 C CNN
F 1 "+12V" H 8915 2823 50  0000 C CNN
F 2 "" H 8900 2650 50  0001 C CNN
F 3 "" H 8900 2650 50  0001 C CNN
	1    8900 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 2650 8900 2800
Wire Wire Line
	6350 2600 6350 2800
$Comp
L power:+5V #PWR?
U 1 1 5E0DACC1
P 2400 3400
F 0 "#PWR?" H 2400 3250 50  0001 C CNN
F 1 "+5V" H 2415 3573 50  0000 C CNN
F 2 "" H 2400 3400 50  0001 C CNN
F 3 "" H 2400 3400 50  0001 C CNN
	1    2400 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 3400 2400 3600
NoConn ~ 2300 3600
NoConn ~ 2700 4000
NoConn ~ 2700 4100
NoConn ~ 2700 4400
NoConn ~ 2700 4600
NoConn ~ 2700 4700
NoConn ~ 2700 4800
NoConn ~ 2700 4900
NoConn ~ 2700 5000
NoConn ~ 2700 5100
NoConn ~ 2700 5200
NoConn ~ 2700 5300
$Comp
L power:GND #PWR?
U 1 1 5E0EB3CC
P 2250 5750
F 0 "#PWR?" H 2250 5500 50  0001 C CNN
F 1 "GND" H 2255 5577 50  0000 C CNN
F 2 "" H 2250 5750 50  0001 C CNN
F 3 "" H 2250 5750 50  0001 C CNN
	1    2250 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 5600 2200 5700
Wire Wire Line
	2200 5700 2250 5700
Wire Wire Line
	2300 5700 2300 5600
Wire Wire Line
	2250 5750 2250 5700
Connection ~ 2250 5700
Wire Wire Line
	2250 5700 2300 5700
Text Notes 2200 1250 0    118  ~ 0
Power Supply
Text Notes 6750 2000 0    118  ~ 0
Motor Controllers
Text Notes 1450 3100 0    50   ~ 0
Add transistors w/pull-ups later
NoConn ~ 1700 5300
NoConn ~ 1700 4600
NoConn ~ 1700 4400
NoConn ~ 1700 4300
NoConn ~ 1700 4200
Text Notes 5900 2300 0    50   ~ 0
Left Motor
Text Notes 8450 2350 0    50   ~ 0
Right Motor
Wire Wire Line
	2800 1500 2800 1700
Wire Wire Line
	2700 1700 2800 1700
Text Label 1200 5000 2    50   ~ 0
~rightSleep
Text Label 1200 5100 2    50   ~ 0
~rightEnable
Text Label 1200 5200 2    50   ~ 0
rightDir
Text Label 1200 4700 2    50   ~ 0
~leftSleep
Text Label 1200 4800 2    50   ~ 0
~leftEnable
Text Label 1200 4900 2    50   ~ 0
leftDir
Wire Wire Line
	1200 4700 1700 4700
Wire Wire Line
	1700 4800 1200 4800
Wire Wire Line
	1200 4900 1700 4900
Wire Wire Line
	1700 5000 1200 5000
Wire Wire Line
	1700 5100 1200 5100
Wire Wire Line
	1700 5200 1200 5200
Text Label 1200 4500 2    50   ~ 0
step
Wire Wire Line
	1200 4500 1700 4500
Text Label 8100 3500 2    50   ~ 0
step
Text Label 5550 3500 2    50   ~ 0
step
Text Label 8100 3200 2    50   ~ 0
~rightSleep
Text Label 8100 3400 2    50   ~ 0
~rightEnable
Text Label 8100 3600 2    50   ~ 0
rightDir
Text Label 5550 3600 2    50   ~ 0
leftDir
Text Label 5550 3400 2    50   ~ 0
~leftEnable
Text Label 5550 3200 2    50   ~ 0
~leftSleep
Wire Wire Line
	5550 3200 5750 3200
Wire Wire Line
	5750 3400 5550 3400
Wire Wire Line
	5550 3500 5750 3500
Wire Wire Line
	5750 3600 5550 3600
Wire Wire Line
	8100 3200 8300 3200
Wire Wire Line
	8300 3400 8100 3400
Wire Wire Line
	8100 3500 8300 3500
Wire Wire Line
	8300 3600 8100 3600
Text Label 1550 4100 2    50   ~ 0
btRx
Text Label 1550 4000 2    50   ~ 0
btTx
Wire Wire Line
	1550 4000 1700 4000
Wire Wire Line
	1700 4100 1550 4100
Text Notes 2350 2950 2    118  ~ 0
Processor
$Comp
L Connector:Conn_01x04_Female J3
U 1 1 5E2DC87B
P 7200 3500
F 0 "J3" H 7228 3430 50  0000 L CNN
F 1 "Conn_01x04_Female" H 7228 3385 50  0001 L CNN
F 2 "" H 7200 3500 50  0001 C CNN
F 3 "~" H 7200 3500 50  0001 C CNN
	1    7200 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J4
U 1 1 5E2DD5B5
P 9900 3500
F 0 "J4" H 9928 3430 50  0000 L CNN
F 1 "Conn_01x04_Female" H 9928 3385 50  0001 L CNN
F 2 "" H 9900 3500 50  0001 C CNN
F 3 "~" H 9900 3500 50  0001 C CNN
	1    9900 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 3400 9700 3400
Wire Wire Line
	9700 3500 9200 3500
Wire Wire Line
	9200 3600 9700 3600
Wire Wire Line
	9700 3700 9200 3700
Wire Wire Line
	7000 3400 6650 3400
Wire Wire Line
	6650 3500 7000 3500
Wire Wire Line
	7000 3600 6650 3600
Wire Wire Line
	6650 3700 7000 3700
Text Notes 7350 3700 0    50   ~ 0
blue\nyellow\ngreen\nred
Text Notes 10050 3700 0    50   ~ 0
blue\nyellow\ngreen\nred
$Comp
L power:+12V #PWR?
U 1 1 5E1C2B43
P 2100 3400
F 0 "#PWR?" H 2100 3250 50  0001 C CNN
F 1 "+12V" H 2115 3573 50  0000 C CNN
F 2 "" H 2100 3400 50  0001 C CNN
F 3 "" H 2100 3400 50  0001 C CNN
	1    2100 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3400 2100 3600
$EndSCHEMATC
