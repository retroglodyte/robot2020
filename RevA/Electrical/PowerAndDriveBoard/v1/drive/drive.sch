EESchema Schematic File Version 4
LIBS:drive-cache
EELAYER 30 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 1 1
Title "Power Supply and Motor Control Board"
Date "2019-12-30"
Rev "001"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:Arduino_Nano_v3.x U2
U 1 1 5E07D7F5
P 2550 4400
F 0 "U2" H 2900 5500 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" H 3200 5400 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 2700 3450 50  0001 L CNN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 2550 3400 50  0001 C CNN
	1    2550 4400
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_A4988 U4
U 1 1 5E080446
P 8700 3500
F 0 "U4" H 9250 4050 50  0000 C CNN
F 1 "Pololu_Breakout_A4988" V 8800 3450 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 8975 2750 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 8800 3200 50  0001 C CNN
	1    8700 3500
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_A4988 U3
U 1 1 5E081523
P 6150 3500
F 0 "U3" H 6650 4050 50  0000 C CNN
F 1 "Pololu_Breakout_A4988" V 6250 3450 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 6425 2750 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 6250 3200 50  0001 C CNN
	1    6150 3500
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:L7805 U1
U 1 1 5E082067
P 2250 1500
F 0 "U1" H 2250 1742 50  0000 C CNN
F 1 "L7805" H 2250 1651 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 2275 1350 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 2250 1450 50  0001 C CNN
	1    2250 1500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J1
U 1 1 5E088206
P 1050 1500
F 0 "J1" H 942 1593 50  0000 C CNN
F 1 "Conn_01x02_Female" H 942 1594 50  0001 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 1050 1500 50  0001 C CNN
F 3 "~" H 1050 1500 50  0001 C CNN
	1    1050 1500
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 5E08A850
P 2750 1300
F 0 "#PWR0101" H 2750 1150 50  0001 C CNN
F 1 "+5V" H 2765 1473 50  0000 C CNN
F 2 "" H 2750 1300 50  0001 C CNN
F 3 "" H 2750 1300 50  0001 C CNN
	1    2750 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5E09C878
P 2250 2100
F 0 "#PWR0102" H 2250 1850 50  0001 C CNN
F 1 "GND" H 2255 1927 50  0000 C CNN
F 2 "" H 2250 2100 50  0001 C CNN
F 3 "" H 2250 2100 50  0001 C CNN
	1    2250 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 1600 1350 1600
Wire Wire Line
	2250 1800 2250 2000
$Comp
L power:+12V #PWR0103
U 1 1 5E08B202
P 1700 1300
F 0 "#PWR0103" H 1700 1150 50  0001 C CNN
F 1 "+12V" H 1715 1473 50  0000 C CNN
F 2 "" H 1700 1300 50  0001 C CNN
F 3 "" H 1700 1300 50  0001 C CNN
	1    1700 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 5E0A0B8C
P 1700 1750
F 0 "C1" H 1818 1796 50  0000 L CNN
F 1 "47uF" H 1818 1705 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 1738 1600 50  0001 C CNN
F 3 "~" H 1700 1750 50  0001 C CNN
	1    1700 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 1900 1700 2000
Wire Wire Line
	1350 1600 1350 2000
$Comp
L Device:CP C3
U 1 1 5E0A392E
P 2750 1750
F 0 "C3" H 2868 1796 50  0000 L CNN
F 1 "0.1uF" H 2868 1705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2788 1600 50  0001 C CNN
F 3 "~" H 2750 1750 50  0001 C CNN
	1    2750 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 1900 2750 2000
Wire Wire Line
	2750 2000 2250 2000
Connection ~ 2250 2000
Wire Wire Line
	2250 2000 2250 2100
Wire Wire Line
	2250 2000 1700 2000
Wire Wire Line
	2550 1500 2750 1500
Wire Wire Line
	2750 1500 2750 1300
Wire Wire Line
	2750 1600 2750 1500
Connection ~ 2750 1500
$Comp
L power:GND #PWR0104
U 1 1 5E0C047A
P 8800 4450
F 0 "#PWR0104" H 8800 4200 50  0001 C CNN
F 1 "GND" H 8805 4277 50  0000 C CNN
F 2 "" H 8800 4450 50  0001 C CNN
F 3 "" H 8800 4450 50  0001 C CNN
	1    8800 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 4300 8700 4400
Wire Wire Line
	8700 4400 8800 4400
Wire Wire Line
	8800 4400 8800 4450
Wire Wire Line
	8900 4300 8900 4400
Wire Wire Line
	8900 4400 8800 4400
Connection ~ 8800 4400
$Comp
L power:GND #PWR0105
U 1 1 5E0BFDE8
P 6250 4500
F 0 "#PWR0105" H 6250 4250 50  0001 C CNN
F 1 "GND" H 6255 4327 50  0000 C CNN
F 2 "" H 6250 4500 50  0001 C CNN
F 3 "" H 6250 4500 50  0001 C CNN
	1    6250 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 4300 6150 4450
Wire Wire Line
	6350 4300 6350 4450
Wire Wire Line
	6150 4450 6250 4450
Wire Wire Line
	6250 4500 6250 4450
Connection ~ 6250 4450
Wire Wire Line
	6250 4450 6350 4450
NoConn ~ 5750 3800
NoConn ~ 5750 3900
NoConn ~ 5750 4000
NoConn ~ 8300 3800
NoConn ~ 8300 3900
NoConn ~ 8300 4000
$Comp
L power:+5V #PWR0106
U 1 1 5E0CF6BB
P 8400 2650
F 0 "#PWR0106" H 8400 2500 50  0001 C CNN
F 1 "+5V" H 8415 2823 50  0000 C CNN
F 2 "" H 8400 2650 50  0001 C CNN
F 3 "" H 8400 2650 50  0001 C CNN
	1    8400 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 3100 8150 3100
Wire Wire Line
	8150 3100 8150 2750
Wire Wire Line
	8150 2750 8400 2750
Wire Wire Line
	8400 2750 8400 2650
Wire Wire Line
	8700 2800 8700 2750
Wire Wire Line
	8700 2750 8400 2750
Connection ~ 8400 2750
$Comp
L power:+5V #PWR0107
U 1 1 5E0D1EC1
P 5850 2550
F 0 "#PWR0107" H 5850 2400 50  0001 C CNN
F 1 "+5V" H 5865 2723 50  0000 C CNN
F 2 "" H 5850 2550 50  0001 C CNN
F 3 "" H 5850 2550 50  0001 C CNN
	1    5850 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3100 5550 3100
Wire Wire Line
	5550 3100 5550 2700
Wire Wire Line
	5550 2700 5850 2700
Wire Wire Line
	6150 2700 6150 2800
Wire Wire Line
	5850 2550 5850 2700
Connection ~ 5850 2700
Wire Wire Line
	5850 2700 6150 2700
$Comp
L power:+12V #PWR0108
U 1 1 5E0D5184
P 6350 2600
F 0 "#PWR0108" H 6350 2450 50  0001 C CNN
F 1 "+12V" H 6365 2773 50  0000 C CNN
F 2 "" H 6350 2600 50  0001 C CNN
F 3 "" H 6350 2600 50  0001 C CNN
	1    6350 2600
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0109
U 1 1 5E0D60B0
P 8900 2650
F 0 "#PWR0109" H 8900 2500 50  0001 C CNN
F 1 "+12V" H 8915 2823 50  0000 C CNN
F 2 "" H 8900 2650 50  0001 C CNN
F 3 "" H 8900 2650 50  0001 C CNN
	1    8900 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 2650 8900 2800
Wire Wire Line
	6350 2600 6350 2800
$Comp
L power:+5V #PWR0110
U 1 1 5E0DACC1
P 2750 3200
F 0 "#PWR0110" H 2750 3050 50  0001 C CNN
F 1 "+5V" H 2765 3373 50  0000 C CNN
F 2 "" H 2750 3200 50  0001 C CNN
F 3 "" H 2750 3200 50  0001 C CNN
	1    2750 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 3200 2750 3400
NoConn ~ 2650 3400
NoConn ~ 2450 3400
NoConn ~ 3050 3800
NoConn ~ 3050 3900
NoConn ~ 3050 4200
NoConn ~ 3050 5000
$Comp
L power:GND #PWR0111
U 1 1 5E0EB3CC
P 2600 5550
F 0 "#PWR0111" H 2600 5300 50  0001 C CNN
F 1 "GND" H 2605 5377 50  0000 C CNN
F 2 "" H 2600 5550 50  0001 C CNN
F 3 "" H 2600 5550 50  0001 C CNN
	1    2600 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 5400 2550 5500
Wire Wire Line
	2550 5500 2600 5500
Wire Wire Line
	2650 5500 2650 5400
Wire Wire Line
	2600 5550 2600 5500
Connection ~ 2600 5500
Wire Wire Line
	2600 5500 2650 5500
$Comp
L Connector:Conn_01x04_Female J2
U 1 1 5E1331AD
P 8950 6100
F 0 "J2" H 8850 6350 50  0000 C CNN
F 1 "Conn_01x04_Female" H 8842 6294 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 8950 6100 50  0001 C CNN
F 3 "~" H 8950 6100 50  0001 C CNN
	1    8950 6100
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5E147DE3
P 3700 1650
F 0 "#PWR0112" H 3700 1400 50  0001 C CNN
F 1 "GND" H 3705 1477 50  0000 C CNN
F 2 "" H 3700 1650 50  0001 C CNN
F 3 "" H 3700 1650 50  0001 C CNN
	1    3700 1650
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0113
U 1 1 5E14859A
P 4300 1350
F 0 "#PWR0113" H 4300 1200 50  0001 C CNN
F 1 "+5V" H 4315 1523 50  0000 C CNN
F 2 "" H 4300 1350 50  0001 C CNN
F 3 "" H 4300 1350 50  0001 C CNN
	1    4300 1350
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0114
U 1 1 5E148B5C
P 4950 1350
F 0 "#PWR0114" H 4950 1200 50  0001 C CNN
F 1 "+12V" H 4965 1523 50  0000 C CNN
F 2 "" H 4950 1350 50  0001 C CNN
F 3 "" H 4950 1350 50  0001 C CNN
	1    4950 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 1250 3700 1250
Wire Wire Line
	3550 1550 3700 1550
Wire Wire Line
	4200 1450 4300 1450
Wire Wire Line
	4300 1450 4300 1350
Wire Wire Line
	4800 1450 4950 1450
Wire Wire Line
	4950 1450 4950 1350
Text Notes 2400 850  0    118  ~ 0
Power Supply
Text Notes 8500 5500 0    118  ~ 0
Bluetooth Module
Text Notes 6750 2000 0    118  ~ 0
Motor Controllers
Text Notes 1800 2900 0    50   ~ 0
Add transistors w/pull-ups later
$Comp
L power:+5V #PWR0115
U 1 1 5E183132
P 9650 5850
F 0 "#PWR0115" H 9650 5700 50  0001 C CNN
F 1 "+5V" H 9665 6023 50  0000 C CNN
F 2 "" H 9650 5850 50  0001 C CNN
F 3 "" H 9650 5850 50  0001 C CNN
	1    9650 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5E183FD9
P 9650 6200
F 0 "#PWR0116" H 9650 5950 50  0001 C CNN
F 1 "GND" H 9655 6027 50  0000 C CNN
F 2 "" H 9650 6200 50  0001 C CNN
F 3 "" H 9650 6200 50  0001 C CNN
	1    9650 6200
	1    0    0    -1  
$EndComp
NoConn ~ 2050 4400
NoConn ~ 2050 4200
NoConn ~ 2050 4000
Text Notes 5900 2300 0    50   ~ 0
Left Motor
Text Notes 8450 2350 0    50   ~ 0
Right Motor
Wire Wire Line
	1700 1300 1700 1500
Wire Wire Line
	1700 2000 1350 2000
Connection ~ 1700 2000
Wire Wire Line
	1250 1500 1700 1500
Connection ~ 1700 1500
Wire Wire Line
	1700 1500 1700 1600
Wire Wire Line
	1950 1500 1700 1500
Text Label 1550 4800 2    50   ~ 0
~rightSleep
Text Label 1550 4900 2    50   ~ 0
~rightEnable
Text Label 1550 5000 2    50   ~ 0
rightDir
Text Label 3300 4400 0    50   ~ 0
~leftSleep
Text Label 3300 4500 0    50   ~ 0
~leftEnable
Text Label 3300 4600 0    50   ~ 0
leftDir
Wire Wire Line
	2050 4800 1550 4800
Wire Wire Line
	2050 4900 1550 4900
Wire Wire Line
	2050 5000 1550 5000
Text Label 1550 4300 2    50   ~ 0
step
Wire Wire Line
	1550 4300 2050 4300
Text Label 8100 3500 2    50   ~ 0
step
Text Label 5550 3500 2    50   ~ 0
step
Text Label 8100 3200 2    50   ~ 0
~rightSleep
Text Label 8100 3400 2    50   ~ 0
~rightEnable
Text Label 8100 3600 2    50   ~ 0
rightDir
Text Label 5550 3600 2    50   ~ 0
leftDir
Text Label 5550 3400 2    50   ~ 0
~leftEnable
Text Label 5550 3200 2    50   ~ 0
~leftSleep
Wire Wire Line
	5550 3200 5750 3200
Wire Wire Line
	5750 3400 5550 3400
Wire Wire Line
	5550 3500 5750 3500
Wire Wire Line
	5750 3600 5550 3600
Wire Wire Line
	8100 3200 8300 3200
Wire Wire Line
	8300 3400 8100 3400
Wire Wire Line
	8100 3500 8300 3500
Wire Wire Line
	8300 3600 8100 3600
Text Label 9300 6300 0    50   ~ 0
btRx
Text Label 9300 6200 0    50   ~ 0
btTx
Text Label 1900 3900 2    50   ~ 0
btRx
Text Label 1900 3800 2    50   ~ 0
btTx
Wire Wire Line
	1900 3800 2050 3800
Wire Wire Line
	2050 3900 1900 3900
Wire Wire Line
	9650 5850 9650 6000
Wire Wire Line
	9150 6000 9650 6000
Wire Wire Line
	9150 6100 9650 6100
Wire Wire Line
	9650 6100 9650 6200
Wire Wire Line
	9300 6200 9150 6200
Wire Wire Line
	9300 6300 9150 6300
Text Notes 2700 2750 2    118  ~ 0
Processor
Text Notes 4850 5700 2    118  ~ 0
Lights
$Comp
L Connector:Conn_01x04_Female J11
U 1 1 5E2DC87B
P 7200 3500
F 0 "J11" H 7228 3430 50  0000 L CNN
F 1 "Conn_01x04_Female" H 7228 3385 50  0001 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-4_P5.08mm" H 7200 3500 50  0001 C CNN
F 3 "~" H 7200 3500 50  0001 C CNN
	1    7200 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J12
U 1 1 5E2DD5B5
P 9900 3500
F 0 "J12" H 9928 3430 50  0000 L CNN
F 1 "Conn_01x04_Female" H 9928 3385 50  0001 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-4_P5.08mm" H 9900 3500 50  0001 C CNN
F 3 "~" H 9900 3500 50  0001 C CNN
	1    9900 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 3400 9700 3400
Wire Wire Line
	9700 3500 9200 3500
Wire Wire Line
	9200 3600 9700 3600
Wire Wire Line
	9700 3700 9200 3700
Wire Wire Line
	7000 3400 6650 3400
Wire Wire Line
	6650 3500 7000 3500
Wire Wire Line
	7000 3600 6650 3600
Wire Wire Line
	6650 3700 7000 3700
Text Notes 5650 6200 2    59   ~ 0
To LED string\n(6 LEDs)
Text Notes 7350 3700 0    50   ~ 0
red\ngreen\nyellow\nblue
Text Notes 10050 3700 0    50   ~ 0
blue\nyellow\ngreen\nred
$Comp
L Transistor_FET:2N7002 Q1
U 1 1 5E1AEAF4
P 4750 6700
F 0 "Q1" H 4956 6746 50  0000 L CNN
F 1 "2N7002" H 4956 6655 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4950 6625 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7002.pdf" H 4750 6700 50  0001 L CNN
	1    4750 6700
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0117
U 1 1 5E1B54A3
P 4850 5950
F 0 "#PWR0117" H 4850 5800 50  0001 C CNN
F 1 "+12V" H 4865 6123 50  0000 C CNN
F 2 "" H 4850 5950 50  0001 C CNN
F 3 "" H 4850 5950 50  0001 C CNN
	1    4850 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 5E1B873E
P 4850 7150
F 0 "#PWR0118" H 4850 6900 50  0001 C CNN
F 1 "GND" H 4855 6977 50  0000 C CNN
F 2 "" H 4850 7150 50  0001 C CNN
F 3 "" H 4850 7150 50  0001 C CNN
	1    4850 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 6900 4850 7150
Text Label 3950 6700 2    50   ~ 0
Headlights
Text Label 3300 4700 0    50   ~ 0
Headlights
$Comp
L Device:R R1
U 1 1 5E1E2B94
P 4250 6700
F 0 "R1" V 4043 6700 50  0000 C CNN
F 1 "100" V 4134 6700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4180 6700 50  0001 C CNN
F 3 "~" H 4250 6700 50  0001 C CNN
	1    4250 6700
	0    1    1    0   
$EndComp
Wire Wire Line
	3950 6700 4100 6700
Wire Wire Line
	4400 6700 4550 6700
$Comp
L Connector:Conn_01x02_Female J13
U 1 1 5E1F2549
P 5050 6150
F 0 "J13" H 5078 6080 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5078 6035 50  0001 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 5050 6150 50  0001 C CNN
F 3 "~" H 5050 6150 50  0001 C CNN
	1    5050 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 5950 4850 6150
Wire Wire Line
	4850 6250 4850 6500
Text Label 3300 4800 0    50   ~ 0
I2C_SDA
Text Label 3300 4900 0    50   ~ 0
I2C_SCL
Wire Wire Line
	3050 4800 3300 4800
Wire Wire Line
	3300 4900 3050 4900
$Comp
L Connector:Conn_01x02_Female J14
U 1 1 5E21B6C9
P 7400 5650
F 0 "J14" H 7428 5580 50  0000 L CNN
F 1 "Conn_01x02_Female" H 7428 5535 50  0001 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 7400 5650 50  0001 C CNN
F 3 "~" H 7400 5650 50  0001 C CNN
	1    7400 5650
	1    0    0    -1  
$EndComp
Text Label 6950 5650 2    50   ~ 0
I2C_SDA
Text Label 6950 5750 2    50   ~ 0
I2C_SCL
Wire Wire Line
	6950 5650 7200 5650
Wire Wire Line
	7200 5750 6950 5750
Text Notes 7800 5500 2    118  ~ 0
I2C Connections
$Comp
L Connector:Conn_01x04_Female J3
U 1 1 5E255005
P 3350 1350
F 0 "J3" H 3242 1543 50  0000 C CNN
F 1 "Conn_01x04_Female" H 3242 1544 50  0001 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-4_P5.08mm" H 3350 1350 50  0001 C CNN
F 3 "~" H 3350 1350 50  0001 C CNN
	1    3350 1350
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J4
U 1 1 5E256A43
P 4000 1450
F 0 "J4" H 3892 1543 50  0000 C CNN
F 1 "Conn_01x02_Female" H 3892 1544 50  0001 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 4000 1450 50  0001 C CNN
F 3 "~" H 4000 1450 50  0001 C CNN
	1    4000 1450
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J5
U 1 1 5E2572D0
P 4600 1450
F 0 "J5" H 4492 1543 50  0000 C CNN
F 1 "Conn_01x02_Female" H 4492 1544 50  0001 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 4600 1450 50  0001 C CNN
F 3 "~" H 4600 1450 50  0001 C CNN
	1    4600 1450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3700 1250 3700 1350
Wire Wire Line
	3550 1350 3700 1350
Connection ~ 3700 1350
Wire Wire Line
	3700 1350 3700 1450
Wire Wire Line
	3550 1450 3700 1450
Connection ~ 3700 1450
Wire Wire Line
	3700 1450 3700 1550
Wire Wire Line
	4800 1550 4950 1550
Wire Wire Line
	4950 1550 4950 1450
Connection ~ 4950 1450
Wire Wire Line
	4200 1550 4300 1550
Wire Wire Line
	4300 1550 4300 1450
Connection ~ 4300 1450
Wire Wire Line
	3700 1650 3700 1550
Connection ~ 3700 1550
Text Notes 7500 900  0    50   ~ 0
70mm between mounting holes
$Comp
L Mechanical:MountingHole H1
U 1 1 5E1BED2D
P 7600 1150
F 0 "H1" H 7700 1196 50  0000 L CNN
F 1 "MountingHole" H 7700 1105 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 7600 1150 50  0001 C CNN
F 3 "~" H 7600 1150 50  0001 C CNN
	1    7600 1150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5E1BF5FB
P 8450 1150
F 0 "H2" H 8550 1196 50  0000 L CNN
F 1 "MountingHole" H 8550 1105 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 8450 1150 50  0001 C CNN
F 3 "~" H 8450 1150 50  0001 C CNN
	1    8450 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 5100 2050 5100
Wire Wire Line
	3050 4400 3300 4400
Wire Wire Line
	3300 4500 3050 4500
Wire Wire Line
	3050 4600 3300 4600
NoConn ~ 2050 4100
NoConn ~ 2050 4500
NoConn ~ 2050 4600
NoConn ~ 2050 4700
Wire Wire Line
	3050 4700 3300 4700
NoConn ~ 3050 5100
$EndSCHEMATC
